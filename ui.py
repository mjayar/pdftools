from PySide6.QtGui import QAction, QIcon
from PySide6.QtPdfWidgets import QPdfView
from PySide6.QtWidgets import (
    QMainWindow,
    QMenu,
    QMenuBar,
    QVBoxLayout,
    QWidget
)


class MainWindowUI:
    def __init__(self, parent: QMainWindow):
        self.action_Open = QAction("&Open", parent)
        self.action_Open.setIcon(QIcon.fromTheme("document-open"))
        self.action_Open.setShortcut("Ctrl+O")

        self.action_Save = QAction("&Save", parent)
        self.action_Save.setIcon(QIcon.fromTheme("document-save"))
        self.action_Save.setShortcut("Ctrl+S")

        self.action_SaveAs = QAction("&Save As", parent)
        self.action_SaveAs.setIcon(QIcon.fromTheme("document-save-as"))
        self.action_SaveAs.setShortcut("Ctrl+Shift+S")

        self.action_Exit = QAction("E&xit", parent)
        self.action_Exit.setIcon(QIcon.fromTheme("application-exit"))
        self.action_Exit.setShortcut("Ctrl+Q")

        self.action_Undo = QAction("&Undo", parent)
        self.action_Undo.setIcon(QIcon.fromTheme("edit-undo"))
        self.action_Undo.setShortcut("Ctrl+Z")

        self.action_Redo = QAction("&Redo", parent)
        self.action_Redo.setIcon(QIcon.fromTheme("edit-redo"))
        self.action_Redo.setShortcut("Ctrl+Y")

        self.action_ViewSource = QAction("&View Source", parent)
        self.action_RequestFeature = QAction("&Request a feature", parent)
        self.action_ReportIssue = QAction("&Report an issue", parent)
        self.action_CheckUpdates = QAction("&Check for updates", parent)

        self.action_About = QAction("&About PDF Tools", parent)
        self.action_About.setIcon(QIcon.fromTheme("help-about"))

        self.menubar = QMenuBar(parent)
        self.menu_File = QMenu("&File", self.menubar)
        self.menu_Edit = QMenu("&Edit", self.menubar)
        self.menu_View = QMenu("&View", self.menubar)
        self.menu_Help = QMenu("&Help", self.menubar)
        parent.setMenuBar(self.menubar)

        self.menubar.addAction(self.menu_File.menuAction())
        self.menubar.addAction(self.menu_Edit.menuAction())
        self.menubar.addAction(self.menu_View.menuAction())
        self.menubar.addAction(self.menu_Help.menuAction())

        self.menu_File.addAction(self.action_Open)
        self.menu_File.addAction(self.action_Save)
        self.menu_File.addAction(self.action_SaveAs)
        self.menu_File.addSeparator()
        self.menu_File.addAction(self.action_Exit)

        self.menu_Edit.addAction(self.action_Undo)
        self.menu_Edit.addAction(self.action_Redo)

        self.menu_Help.addAction(self.action_ViewSource)
        self.menu_Help.addAction(self.action_RequestFeature)
        self.menu_Help.addAction(self.action_ReportIssue)
        self.menu_Help.addSeparator()
        self.menu_Help.addAction(self.action_CheckUpdates)
        self.menu_Help.addSeparator()
        self.menu_Help.addAction(self.action_About)

        self.central_Widget = QWidget(parent)
        parent.setCentralWidget(self.central_Widget)

        self.main_VBox = QVBoxLayout(self.central_Widget)
        self.main_VBox.setSpacing(0)
        self.main_VBox.setContentsMargins(0, 0, 0, 0)

        self.pdfViewer = QPdfView(self.central_Widget)
        self.main_VBox.addWidget(self.pdfViewer)
