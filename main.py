from argparse import ArgumentParser, RawTextHelpFormatter
import webbrowser
import sys


from PySide6.QtCore import QPoint, QStandardPaths, QUrl,  Slot
from PySide6.QtPdf import QPdfDocument
from PySide6.QtPdfWidgets import QPdfView
from PySide6.QtWidgets import (
    QApplication,
    QFileDialog,
    QMainWindow,
    QMessageBox
)

from ui import MainWindowUI


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("PDF Tools")
        self.setMinimumSize(400, 300)
        self.resize(800, 600)
        self.ui = MainWindowUI(self)

        self.file_Dialog = None

        self.document = QPdfDocument(self)
        self.ui.pdfViewer.setDocument(self.document)
        self.ui.pdfViewer.setPageMode(QPdfView.PageMode.MultiPage)

        self.ui.action_Open.triggered.connect(self.on_action_Open_triggered)
        self.ui.action_Exit.triggered.connect(lambda: self.close())
        self.ui.action_ViewSource.triggered.connect(lambda: webbrowser.open(
            "https://gitlab.com/mjayar/pdftools", new=0, autoraise=True))
        self.ui.action_About.triggered.connect(lambda: QMessageBox.about(
            self, "About PDF Tools", "PDF Tools v0.1 developed by Mark Devilleres"))

    @Slot(QUrl)
    def open(self, filePath: QUrl):
        if filePath.isLocalFile():
            self.document.load(filePath.toLocalFile())
            title = self.document.metaData(QPdfDocument.MetaDataField.Title)
            self.setWindowTitle(title if title else "PDF Tools")
            self.goto(0)
        else:
            message = f"File {filePath.toString()} does not exist."
            print(message, file=sys.stderr)
            QMessageBox.critical(self, "Error opening file", message)

    @Slot(int)
    def goto(self, page: int):
        nav = self.ui.pdfViewer.pageNavigator()
        nav.jump(page, QPoint(), nav.currentZoom())

    @Slot()
    def on_action_Open_triggered(self):
        if not self.file_Dialog:
            directory = QStandardPaths.writableLocation(
                QStandardPaths.DocumentsLocation
            )
            self.file_Dialog = QFileDialog(self, "Open a PDF file", directory)
            self.file_Dialog.setAcceptMode(QFileDialog.AcceptOpen)
            self.file_Dialog.setMimeTypeFilters(["application/pdf"])

        if self.file_Dialog.exec() == QFileDialog.Accepted:
            fileUrl = self.file_Dialog.selectedUrls()[0]
            if fileUrl.isValid():
                self.open(fileUrl)


if __name__ == "__main__":
    parser = ArgumentParser(
        description="PDF Tools - A simple PDF viewer and editor",
        formatter_class=RawTextHelpFormatter
    )
    parser.add_argument("file", nargs="?", type=str, help="PDF file to open")
    options = parser.parse_args()

    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()

    if options.file:
        window.open(QUrl.fromLocalFile(options.file))

    sys.exit(app.exec())
